'use strict'

var gulp = require('gulp')
var $ = require('gulp-load-plugins')({lazy: false})
var del = require('del')
var merge = require('merge-stream')

var config = {
  bootstrapDir: './bower_components/bootstrap-sass-official',
  distDir: './dist',
  docsDir: './docs'
}

// Tasks:
// - (sass) Compile Sass to CSS
// - (js) Compile Javascript
// - (svg) Minify SVGs?
// - (build-docs) Build Documentation
//   - (jade) Compile Jade to HTML
//
// - (distToDocs) Copy CSS and JS from Dist to Docs folder
// - (minify) Minify CSS & JS
//   - (minify-css) Minify CSS
//   - (minify-js) Minify JS
// - (clean) Clear out dist folder to ready for recompile
// - (serve) Local web server for documentation on 8888
//   - (watch) Watch for changes and reload site
//
// - (rebuild) ???
// - (default) ???
// - (connect) ???
// - (debug) ???

//
// (sass)
gulp.task('sass', ['svg'], function () {
  return gulp.src('./src/sass/ua-bootstrap.scss')
    .pipe($.sass({
      includePaths: [config.bootstrapDir + '/assets/stylesheets']
    }))
    .pipe($.inlineImage())
    .pipe($.autoprefixer('last 2 versions'))
    .pipe(gulp.dest(config.distDir + '/css'))
})

gulp.task('minify-js', function () {
  return gulp.src(config.distDir + '/js/ua-bootstrap.js')
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.uglify())
    .pipe(gulp.dest(config.distDir + '/js'))
})

gulp.task('minify-css', ['sass'], function () {
  return gulp.src(config.distDir + '/css/ua-bootstrap.css')
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.minifyCss({processImport: false}))
    .pipe(gulp.dest(config.distDir + '/css'));
})

gulp.task('minify', ['minify-css', 'minify-js'])

//
// (js)
gulp.task('js', function () {
  return gulp.src([
    config.bootstrapDir + '/assets/javascripts/**/*',
    './src/js/*.js'
  ])
    .pipe($.concat('ua-bootstrap.js'))
    .pipe(gulp.dest(config.distDir + '/js/'))
})

//
// (svg)
gulp.task('svg', function () {
  return gulp.src('./src/svg/*.svg')
    .pipe($.svgmin())
    .pipe(gulp.dest('./src/img/.'))
})

//
// (jade)
gulp.task('jade', function () {
  return gulp.src(config.docsDir + '/jade/*.jade')
    .pipe($.jade({
      pretty: true
    }))
    .pipe(gulp.dest(config.docsDir))
})

//
// (build-docs)
gulp.task('build-docs', function () {
  var fonts = gulp.src('ua_components/ua-brand-icons/fonts/ua-brand-symbols.*')
    .pipe(gulp.dest('docs/ua-brand-icons/fonts'))
  var icons = gulp.src('ua_components/ua-brand-icons/*.css')
    .pipe(gulp.dest('docs/ua-brand-icons'))
  var hjs = gulp.src('bower_components/highlightjs/*.js')
    .pipe(gulp.dest('docs/js'))
  var holderjs = gulp.src('bower_components/holderjs/*.js')
    .pipe(gulp.dest('docs/js'))
  var gc = gulp.src('bower_components/highlightjs/styles/googlecode.css')
    .pipe(gulp.dest('docs/css'))
  return merge(fonts, icons, hjs, gc, holderjs)
})

//
// - (distToDocs)
gulp.task('distToDocs', ['minify'], function () {
  return gulp.src(config.distDir + '/*/**')
  .pipe(gulp.dest(config.docsDir))
})

//
// (clean)
gulp.task('clean', function (cb) {
  del(['dist/*'], cb)
})

//
// (rebuild)
gulp.task('rebuild', ['svg', 'jade', 'js', 'sass', 'build-docs', 'distToDocs'])

//
// (default)
gulp.task('default', ['clean'], function () {
  gulp.start('rebuild')
})

//
// (connect)
gulp.task('connect', function () {
  $.connect.server({
    port: 8888,
    root: 'docs',
    livereload: true
  })
})

//
// (clean)
//
gulp.task('watch', function () {
  $.livereload.listen()
  gulp.watch([
    'src/sass/**/**/**',
    'src/svg/**',
    'src/js/**',
    'docs/jade/**/**/**'], ['rebuild'])
    .on('change', $.livereload.changed)
})

//
// (serve)
gulp.task('serve', ['connect', 'watch'])

//
// (debug)
gulp.task('debug', function () {
  console.log($)
})
