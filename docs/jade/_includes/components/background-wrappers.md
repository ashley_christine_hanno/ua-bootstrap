<h1 id="background-wrappers" class="page-header">Background Wrappers</h1>
<p class="lead">Sometimes you just want the background to be full width in certain sections.</p>
<p>For full width backgrounds, you must close the`.row`, and `.container` and then wrap a new `.container` `<div>` in another `<div>` with the `.background-wrapper` class. Since `<div>`s go full width naturally, you'll know when you haven't closed all structural `<div>`s when the wrapper does not span the width of the page.</p>
<h2 id="background-wrapper-basic">Basic example</h2>
<p>By default, the background-wrapper class just adds some `padding` to the top
and bottom of the wrapper `<div>`.</p>
</div></div></div></div>
<div class="example" data-example-id="simple-background-wapper">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="background-wrapper bg-silver-tint">
  <div class="container bs-docs-container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        <h2 class="serif">The University of Arizona is committed to achieving
        full accessibility of all electronic and information technology to
        ensure equitable experiences for everyone</h2>
        </div>
      </div>
      <div class="row show-grid">
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
        <div class="col-md-1">.col-md-1</div>
      </div>
    </div>
  </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close Column-->
  </div><!--Close Row-->
</div><!--Close Container-->
<div class="background-wrapper bg-silver-tint">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
    <div class="row">
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
    </div>
  </div>
</div> <!--Close Wrapper-->
```
<h2 id="background-wrapper-triangle-fade">Triangle Fade</h2>
<p>We have also created some repeating svg based backgrounds you can use. SVGs
can be edited in Adobe Illustrator, or even in your favorite text editor.</p>
</div></div></div></div>
<div class="example" data-example-id="simple-background-wapper">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="background-wrapper bg-triangles-fade bg-ash-tint">
  <div class="container bs-docs-container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        <h2 class="serif">The University of Arizona is committed to achieving
        full accessibility of all electronic and information technology to
        ensure equitable experiences for everyone</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-fade bg-ash-tint">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h2 id="background-wrapper-catalinas-abstract">Catalinas Abstract</h2>
<p>A representation of the Catalina Mountains, this graphic has vertical fadeout.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <h3 class="example-label"><span class="label label-info">Example</span></h3>
      <div class="background-wrapper bg-catalinas-abstract">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">The University of Arizona is committed to achieving
            full accessibility of all electronic and information technology to
            ensure equitable experiences for everyone</h2>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-catalinas-abstract">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```
<h2 id="background-wrapper-triangle-mosaic">Triangle Mosaic</h2>
<p>Triangle mosaic is translucent so you can select the background color you
would like to use.</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <h3 class="example-label"><span class="label label-info">Example</span></h3>
      <div class="background-wrapper bg-triangles-mosaic bg-sky">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif">The University of Arizona is committed to achieving
            full accessibility of all electronic and information technology to
            ensure equitable experiences for everyone</h2>
            </div>
          </div>
        </div>
      </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-wrapper bg-triangles-mosaic bg-sky">
  <div class="container">
    <div class="row">
      <div class="col-md-9 text-center-not-xs">
        ...
      </div>
    </div>
  </div>
</div> <!--Close wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```

<h2 id="background-wrapper-layered-images">Layered Images</h2>
<p>You can even use images under the backgrounds</p>
</div></div>
  </div>
    <div class="bs-docs-section">
        <div class="example" data-example-id="simple-background-wapper">
          <h3 class="example-label"><span class="label label-info">Example</span></h3>
    <div class="background-image-wrapper">
      <div class="background-wrapper bg-triangles-translucent">
      <div class="container bs-docs-container">
        <div class="row">
          <div class="col-md-9 text-center-not-xs">
            <h2 class="serif"><br><br><br><br></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="container bs-docs-container">
  <div class="row">
    <div class="col-md-9">
```html
<div class="container">
  <div class="row">
    <div class="col-md-9">
    </div><!--Close column-->
  </div><!--Close row-->
</div><!--Close container-->
<div class="background-image-wrapper">
  <div class="background-wrapper bg-triangles-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-9 text-center-not-xs">
          ...
        </div>
      </div>
    </div>
  </div> <!--Close wrapper-->
</div> <!--Close image wrapper-->
<div class="container"> <!-- Restart regular columns -->
  <div class="row">
    <div class="col-md-9">
```