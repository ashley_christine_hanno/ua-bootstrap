<h1 id="breadcrumbs" class="page-header">Breadcrumbs</h1>

<p class="lead">Indicate the current page's location within a navigational hierarchy.</p>
<p>Separators are automatically added in CSS through <code>:before</code> and <code>content</code>.</p>
<div class="example" data-example-id="simple-breadcrumbs">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <ol class="breadcrumb">
    <li class="active">Home</li>
  </ol>
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Library</li>
  </ol>
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Library</a></li>
    <li class="active">Data</li>
  </ol>
</div>

```html
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Library</a></li>
<li class="active">Data</li>
</ol>
```
