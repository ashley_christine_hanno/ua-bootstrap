<h1 id="wells" class="page-header">Wells</h1>

<h2>Default well</h2>
<p>Use the well as a simple effect on an element to give it an inset effect.</p>
<div class="example" data-example-id="default-well">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="well">
    Look, I'm in a well!
  </div>
</div>

```html
<div class="well">...</div>
```
<h2>Optional classes</h2>
<p>Control padding and rounded corners with two optional modifier classes.</p>
<div class="example" data-example-id="large-well">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="well well-lg">
    Look, I'm in a large well!
  </div>
</div>

```html
<div class="well well-lg">...</div>
```

<div class="example" data-example-id="small-well">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="well well-sm">
    Look, I'm in a small well!
  </div>
</div>

```html
<div class="well well-sm">...</div>
```
