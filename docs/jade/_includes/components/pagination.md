<h1 id="pagination" class="page-header">Pagination</h1>
<p class="lead">Provide pagination links for your site or app with the multi-page pagination component, or the simpler <a href="#pagination-pager">pager alternative</a>.</p>
<h2 id="pagination-default">Default pagination</h2>
<p>Simple pagination inspired by Rdio, great for apps and search results. The large block is hard to miss, easily scalable, and provides large click areas.</p>

<div class="example" data-example-id="simple-pagination">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pagination">
      <li>
        <a href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li>
        <a href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>
    </ul>
  </nav>
</div>

```html
<nav>
<ul class="pagination">
  <li>
    <a href="#" aria-label="Previous">
      <span aria-hidden="true">&laquo;</span>
    </a>
  </li>
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li>
    <a href="#" aria-label="Next">
      <span aria-hidden="true">&raquo;</span>
    </a>
  </li>
</ul>
</nav>
```

<h3>Disabled and active states</h3>
<p>Links are customizable for different circumstances. Use <code>.disabled</code> for unclickable links and <code>.active</code> to indicate the current page.</p>

<div class="example" data-example-id="disabled-active-pagination">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pagination">
      <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
      <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
   </ul>
 </nav>
</div>

```html
<nav>
<ul class="pagination">
  <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
  <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
  ...
</ul>
</nav>
```

<p>You can optionally swap out active or disabled anchors for <code>&lt;span&gt;</code>, or omit the anchor in the case of the previous/next arrows, to remove click functionality while retaining intended styles.</p>

```html
<nav>
<ul class="pagination">
  <li class="disabled">
    <span>
      <span aria-hidden="true">&laquo;</span>
    </span>
  </li>
  <li class="active">
    <span>1 <span class="sr-only">(current)</span></span>
  </li>
  ...
</ul>
</nav>
```

<h3>Sizing</h3>
<p>Fancy larger or smaller pagination? Add <code>.pagination-lg</code> or <code>.pagination-sm</code> for additional sizes.</p>

<div class="example" data-example-id="pagination-sizing">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pagination pagination-lg">
      <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
    </ul>
  </nav>
  <nav>
    <ul class="pagination">
      <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
    </ul>
  </nav>
  <nav>
    <ul class="pagination pagination-sm">
      <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
    </ul>
  </nav>
</div>

```html
<nav><ul class="pagination pagination-lg">...</ul></nav>
<nav><ul class="pagination">...</ul></nav>
<nav><ul class="pagination pagination-sm">...</ul></nav>
```

<h2 id="pagination-pager">Pager</h2>
<p>Quick previous and next links for simple pagination implementations with light markup and styles. It's great for simple sites like blogs or magazines.</p>

<h3>Default example</h3>
<p>By default, the pager centers links.</p>
<div class="example" data-example-id="simple-pager">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pager">
      <li><a href="#"><span aria-hidden="true">&laquo;</span> Previous</a></li>
      <li><a href="#">Next <span aria-hidden="true">&raquo;</span></a></li>
    </ul>
  </nav>
</div>

```html
<nav>
<ul class="pager">
  <li><a href="#"><span aria-hidden="true">&laquo;</span> Previous</a></li>
  <li><a href="#">Next <span aria-hidden="true">&raquo;</span></a></li>
</ul>
</nav>
```

<h3>Aligned links</h3>
<p>Alternatively, you can align each link to the sides:</p>
<div class="example" data-example-id="aligned-pager-links">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pager">
      <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
      <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
    </ul>
  </nav>
</div>

```html
<nav>
<ul class="pager">
  <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
  <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
</ul>
</nav>
```

<h3>Optional disabled state</h3>
<p>Pager links also use the general <code>.disabled</code> utility class from the pagination.</p>
<div class="example" data-example-id="disabled-pager">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <nav>
    <ul class="pager">
      <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
      <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
    </ul>
  </nav>
</div>

```html
<nav>
<ul class="pager">
  <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
  <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
</ul>
</nav>
```
