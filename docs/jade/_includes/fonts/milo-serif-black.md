###  Milo Serif Black

<button class="js-specimen-modal-trigger btn btn-red pull-right" data-font-class='milo-serif-web-black' data-font-name='Milo Serif Black' data-target='.bs-example-modal-lg' data-toggle='modal' type='button'>View Sample</button>
### Serif Normal Black

<style>
.serif-normal-black {
    font-weight: 900;
    font-family: MiloSerifWeb-Black, TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif;
    font-size: 36px;
}
.serif-normal-black span:first-child {
    text-transform: uppercase;
}
</style>

<div class="serif-normal-black">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>

The font family changes to **MiloSerifWeb-Black** <span class="label label-primary">Notice</span>

```css
.serif-normal-black {
  font-family: MiloSerifWeb-Black, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
  font-style: normal;
  font-weight: 900;
}

<span class="serif-normal-black">
    ...
</span>
```

<button class="js-specimen-modal-trigger btn btn-red pull-right" data-font-class='milo-serif-web-black-italics'  data-font-name='Milo Serif Black Italic'  data-target='.bs-example-modal-lg'  data-toggle='modal'  type='button'>View Sample</button>
### Serif Normal Black Italic

<style>
.serif-normal-black-italic {
    font-weight: normal;
    font-family: MiloSerifWeb-Black, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
    font-style: italic;
    font-size: 36px;
}
</style>

<div class="serif-normal-black-italic">
  <div class="example">
   <h3 class="example-label"><span class="label label-info">Example</span></h3>
   <span class="text-uppercase">abcdefghijklmnopqrstuvwxyz</span>
   <span>abcdefghijklmnopqrstuvwxyz</span> <br />
   <span>0123456789</span> <br />
   <span>!@#$%^&</span>
 </div>
</div>

```css
.serif-normal-black-italic {
  font-family: MiloSerifWeb-Black, TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;
  font-style: normal;
  font-weight:normal;
  font-style: italic;
}

<span class="serif-normal-black-italic">
    ...
</span>
```
