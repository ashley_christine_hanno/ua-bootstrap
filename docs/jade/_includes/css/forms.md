<a id="css-forms"></a>
## Forms

<a id="css-forms-basic-example"></a>
### Basic Example

Individual form controls automatically receive some global styling. All textual
`<input>`, `<textarea>`, and `<select>` elements with `.form-control` are set
to width: 50%; by default. Wrap labels and controls in `.form-group` for
optimum spacing.

<div class="example">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="input-group">
    <label> Checkboxes
      <span class="form-required" title="This field is required.">*</span>
    </label>
    <div class="checkbox">
      <input type="checkbox" value="" id="custom-checkbox-1">
      <label for="custom-checkbox-1">Check box</label>
    </div>
  </div>
  <div class="input-group">
    <label> Radios
      <span class="form-required" title="This field is required.">*</span>
    </label>
    <div class="radio">
      <input type="radio" name="optionsRadios" id="custom-radio-1" value="option1" checked>
      <label for="custom-radio-1"> Radio button </label>
    </div>
  </div>
  <button type="submit" class="btn btn-red">Submit</button>
</form>
</div>

```html
<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="input-group">
    <label> Checkboxes
      <span class="form-required" title="This field is required.">*</span>
    </label>
    <div class="checkbox">
      <input type="checkbox" value="" id="custom-checkbox-1">
      <label for="custom-checkbox-1">Check box</label>
    </div>
  </div>
  <div class="input-group">
    <label> Radios
      <span class="form-required" title="This field is required.">*</span>
    </label>
    <div class="radio">
      <input type="radio" name="optionsRadios" id="custom-radio-1" value="option1" checked>
      <label for="custom-radio-1"> Radio button </label>
    </div>
  </div>
  <button type="submit" class="btn btn-red">Submit</button>
</form>
```