<a id="css-tables"></a>
## Tables

<a id="css-tables-basic"></a>
### Basic

Use the standard table code to make tables.
<div class="example">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <table class="table-striped">
    <caption>Optional table caption.</caption>
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>URL</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>1</th>
        <td>University of Arizona</td>
        <td>arizona.edu</td>
        <td>@UofA</td>
      </tr>
      <tr>
        <th>2</th>
        <td>Arizona Alumni</td>
        <td>arizonaalumni.com</td>
        <td>@UAAA</td>
      </tr>
      <tr>
        <th>3</th>
        <td>Arizona Athletics</td>
        <td>arizonawildcats.com</td>
        <td>@AZATHLETICS</td>
      </tr>
    </tbody>
  </table>
</div>

```html
<table class="table-striped">
    ...
</table>
```

<a id="css-tables-hover"></a>
### Hover Rows

Add the `.table-hover` class to table elements for a blue hover effect on table rows.


<div class="example">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <table class="table-hover table-striped">
    <caption>Optional table caption.</caption>
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>URL</th>
        <th>Username</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>1</th>
        <td>University of Arizona</td>
        <td>arizona.edu</td>
        <td>@UofA</td>
      </tr>
      <tr>
        <th>2</th>
        <td>Arizona Alumni</td>
        <td>arizonaalumni.com</td>
        <td>@UAAA</td>
      </tr>
      <tr>
        <th>3</th>
        <td>Arizona Athletics</td>
        <td>arizonawildcats.com</td>
        <td>@AZATHLETICS</td>
      </tr>
    </tbody>
  </table>
</div>

```html
<table class="table-hover table-striped">
    ...
</table>
```

<a id="css-tables-responsive"></a>
### Responsive Tables

Create responsive tables by wrapping any `<table>` in `.table-responsive` to
make them scroll horizontally on small devices (under 48em). When viewing on
anything larger than 48em wide, you will not see any difference in these
tables.

<div class="example">
  <h3 class="example-label"><span class="label label-info">Example</span></h3>
  <div class="table-responsive">
    <table class="table-responsive table-striped">
      <caption>Optional table caption.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>URL</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>University of Arizona</td>
          <td>arizona.edu</td>
          <td>@UofA</td>
        </tr>
        <tr>
          <th>2</th>
          <td>Arizona Alumni</td>
          <td>arizonaalumni.com</td>
          <td>@UAAA</td>
        </tr>
        <tr>
          <th>3</th>
          <td>Arizona Athletics</td>
          <td>arizonawildcats.com</td>
          <td>@AZATHLETICS</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

```html
<table class="table-responsive table-striped">
    ...
</table>
```
