<a id="grid"></a>
## Grid system

<p class='lead'>
Bootstrap includes a responsive, mobile first fluid grid system that
appropriately scales up to 12 columns as the device or viewport size increases.
It includes **[predefined classes](#grid-example-basic)** for easy layout options.</p>

<a id="grid-intro"></a>
### Introduction

Grid systems are used for creating page layouts through a series of rows and
columns that house your content. Here's how the Bootstrap grid system works:

- Rows must be placed within a `.container` (fixed-widt) or `.container-fluid`
  (full-width) for proper alignment and padding.
- Use rows to create horizontal groups of columns.
- Content should be placed within columns, and only columns may be immediate
  children of rows.
- Predefined grid classes like `.row` and `.col-xs-4` are available for quickly
  making grid layouts.
- Columns create gutters (gaps between column content) via `padding`. That
  padding is offset in rows for the first and last column via negative margin
  with elements with a class of `.row`.
- The negative margin is why the examples below are outdented. It's so that
  content within grid columns is lined up with non-grid content.
- Grid columns are created by specifying the number of twelve available columns
  you wish to span. For example, three equal columns would use three
  `.col-xs-4.`
- If more than 12 columns are placed within a single row, each group of extra
  columns will, as one unit, wrap onto a new line.
- Grid classes apply to devices with screen widths greater than or equal to
  the breakpoint sizes, and override grid classes targeted at smaller devices.
  Therefore, e.g. applying any `.col-md-*` class to an element will not only
  affect its styling on medium devices but also on large devices if
  a `.col-lg-*` class is not present.

Look to the examples for applying these principles to your code.

<a id="grid-options"></a>
### Grid options

See how aspects of the Bootstrap grid system work across multiple devices with
a handy table.

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Extra small devices<small><br>Phones (&lt;48em)</small></th>
                <th>Small devices<small><br>Tablets (≥48em)</small></th>
                <th>Medium devices<small><br>Desktops (≥60em)</small></th>
                <th>Large devices<small><br>Desktops (≥75em)</small></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row" class="text-nowrap">Grid behavior</th>
                <td>Horizontal at all times</td>
                <td colspan="3">Collapsed to start, horizontal above breakpoints</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Container width</th>
                <td>None (auto)</td>
                <td>46.75em</td>
                <td>58em</td>
                <td>72.75em</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Class prefix</th>
                <td><code>.col-xs-</code></td>
                <td><code>.col-sm-</code></td>
                <td><code>.col-md-</code></td>
                <td><code>.col-lg-</code></td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap"># of columns</th>
                <td colspan="4">12</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Column width</th>
                <td class="text-muted">Auto</td>
                <td>~3.875em</td>
                <td>~5em</td>
                <td>~6em</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Gutter width</th>
                <td colspan="4">2em (1em on each side of a column)</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Nestable</th>
                <td colspan="4">Yes</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Offsets</th>
                <td colspan="4">Yes</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">Column ordering</th>
                <td colspan="4">Yes</td>
            </tr>
        </tbody>
    </table>
</div>


<a id="grid-example-basic"></a>
### Example: Stacked-to-horizontal
Using a single set of `.col-md-*` grid classes, you can create a basic grid
system that stars out stacked on mobile devices and tablet devices (the extra
small to small range) before becoming horizontal on desktop (medium) devices.
Place grid columns in any `.row`.
<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
  <div class="col-md-1">.col-md-1</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-8">.col-md-8</div>
  <div class="col-md-4">.col-md-4</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4">.col-md-4</div>
 </div>
 <div class="row show-grid">
  <div class="col-md-6">.col-md-6</div>
  <div class="col-md-6">.col-md-6</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
    <div class="col-md-1">.col-md-1</div>
</div>
<div class="row">
     <div class="col-md-8">.col-md-8</div>
     <div class="col-md-4">.col-md-4</div>
</div>
<div class="row">
    <div class="col-md-4">.col-md-4</div>
    <div class="col-md-4">.col-md-4</div>
    <div class="col-md-4">.col-md-4</div>
</div>
<div class="row">
    <div class="col-md-6">.col-md-6</div>
    <div class="col-md-6">.col-md-6</div>
</div>
```
<a id="grid-example-fluid"></a>

### Example: Fluid container

Turn any fixed-width grid layout into a full-width layout by changing your
outermost `.container` to `.container-fluid`

```html
<div class="container-fluid">
  <div class="row">
      ...
  </div>
</div>
```

<a id="grid-example-mixed"></a>
### Example: Mobile and desktop

Don&apos;t want your columns to simply stack in smaller devices? Use the extra
small and medium device grid classes by adding `.col-xs-*` `.col-md-*` to your
columns. See the example below for a better idea of how it all works.

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>

 <div class="row show-grid">
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>

 <div class="row show-grid">
  <div class="col-xs-6">.col-xs-6</div>
  <div class="col-xs-6">.col-xs-6</div>
 </div>
</div>

```html
<!-- Stack the columns on mobile by making one full-width and the other half-width -->
<div class="row">
 <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>

<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<div class="row">
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>

<!-- Columns are always 50% wide, on mobile and desktop -->
<div class="row">
 <div class="col-xs-6">.col-xs-6</div>
 <div class="col-xs-6">.col-xs-6</div>
</div>
```

<a id="grid-example-mixed-complete"></a>
### Example: Mobile, tablet, desktop

Build on the previous example by creating even more dynamic and powerful layouts
with tablet `.col-sm-*` classes.

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
     <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
     <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
 </div>
 <div class="row show-grid">
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
     <!-- Optional: clear the XS cols if their content doesn't match in height -->
     <div class="clearfix visible-xs-block"></div>
     <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
    <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>
<div class="row">
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
    <!-- Optional: clear the XS cols if their content doesn't match in height -->
    <div class="clearfix visible-xs-block"></div>
    <div class="col-xs-6 col-sm-4">.col-xs-6 .col-sm-4</div>
</div>
```

<a id="grid-example-wrapping"></a>
### Example: Column wrapping


If more than 12 columns are placed within a single row, each group of extra
columns will, as one unit, wrap onto a new line.

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
   <div class="col-xs-9">.col-xs-9</div>
   <div class="col-xs-4">.col-xs-4<br>Since 9 + 4 = 13 > 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
   <div class="col-xs-6">.col-xs-6<br>Subsequent columns continue along the new line.</div>
 </div>
</div>

```html
<div class="row">
  <div class="col-xs-9">.col-xs-9</div>
  <div class="col-xs-4">.col-xs-4<br>Since 9 + 4 = 13 > 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
  <div class="col-xs-6">.col-xs-6<br>Subsequent columns continue along the new line.</div>
</div>
```

<a id="grid-responsive-resets"></a>
### Responsive column resets

With the four tiers of grids available you're bound to run into issues where, at
certain breakpoints, your columns don't clear quite right as one is taller than
the other. To fix that, use a combination of a `.clearfix` and our [responsive
utility classes](#responsive-utilities).

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
 </div>
</div>

```html
<div class="row">
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>

  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>

  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
  <div class="col-xs-6 col-sm-3">.col-xs-6 .col-sm-3</div>
</div>
```

In addition to column clearing at responsive breakpoints, you may need to
**reset offsets, pushes, or pulls**. See this in action in the [grid example](/grids.html).

<a id="grid-offsetting"></a>
### Offsetting columns

Move columns to the right using `.col-md-offset-*` classes. These classes
increase the left margin of a column by `*` columns. For example,
`.col-md-offset-4` moves `.col-md-4` over four columns.

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-sm-5 col-md-6">.col-sm-5 .col-md-6</div>
  <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">.col-sm-5 .col-sm-offset-2 .col-md-6 .col-md-offset-0</div>
 </div>

 <div class="row show-grid">
  <div class="col-sm-6 col-md-5 col-lg-6">.col-sm-6 .col-md-5 .col-lg-6</div>
  <div class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">.col-sm-6 .col-md-5 .col-md-offset-2 .col-lg-6 .col-lg-offset-0</div>
 </div>
</div>

```html
<div class="row">
    <div class="col-sm-5 col-md-6">.col-sm-5 .col-md-6</div>
    <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0">.col-sm-5 .col-sm-offset-2 .col-md-6 .col-md-offset-0</div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-5 col-lg-6">.col-sm-6 .col-md-5 .col-lg-6</div>
    <div class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">.col-sm-6 .col-md-5 .col-md-offset-2 .col-lg-6 .col-lg-offset-0</div>
</div>
```

<a id="grid-nesting"></a>
### Nesting columns

To nest your content with the default grid, add a new `.row` and set of
`.col-sm-*` columns within an existing `.col-sm-*` column. Nested rows should
include a set of columns that add up to 12 or fewer (it is not required that you
use all 12 available columns).

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-sm-9">
   Level 1: .col-sm-9
   <div class="row show-grid">
   <div class="col-xs-8 col-sm-6">
     Level 2: .col-xs-8 .col-sm-6
   </div>
   <div class="col-xs-4 col-sm-6">
     Level 2: .col-xs-4 .col-sm-6
   </div>
   </div>
  </div>
 </div>
</div>

```html
<div class="row">
 <div class="col-sm-9">
   Level 1: .col-sm-9
   <div class="row">
     <div class="col-xs-8 col-sm-6">
       Level 2: .col-xs-8 .col-sm-6
     </div>
     <div class="col-xs-4 col-sm-6">
       Level 2: .col-xs-4 .col-sm-6
     </div>
   </div>
 </div>
</div>
```

<a id="grid-column-ordering"></a>
### Column ordering

Easily change the order of our built-in grid columns with `.col-md-push-*` and
`.col-md-pull-*` modifier classes.

<div class="example">
 <h3 class="example-label"><span class="label label-info">Example</span></h3>
 <div class="row show-grid">
  <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
</div>
</div>

```html
<div class="row">
  <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
</div>
```

