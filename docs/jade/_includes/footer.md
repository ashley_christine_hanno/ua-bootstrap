<footer role="contentinfo" class="bs-footer">
  <div class="container text-center">
    <hr>
    <p><a href="http://getbootstrap.com" target="_blank">Twitter Bootstrap</a> originally designed and built by&nbsp;<a href="http://twitter.com/mdo" target="_blank">@mdo</a>&nbsp;and&nbsp;<a href="http://twitter.com/fat" target="_blank">@fat</a>.</p>
    <p>The University of Arizona flavor built and maintained by the UA Digital group.</p>
    <p>Code licensed under&nbsp;<a href="https://github.com/twbs/bootstrap/blob/master/LICENSE" target="_blank">MIT</a>, documentation under&nbsp;<a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
    <ul class="footer-links muted list-inline list-unstyled">
      <li>Currently v1.0.0-alpha3</li>
      <li>·</li>
      <li><a href="https://bitbucket.org/uadigital/ua-bootstrap" target="_blank">Bitbucket</a></li>
      <li>·</li>
      <li><a href="https://jira.arizona.edu/browse/UADIGITAL-145?jql=component%20%3D%20%22UA%20Bootstrap%22%20AND%20project%20%3D%20UADIGITAL" target="_blank">Issues</a></li>
      <li>·</li>
      <li><a href="https://bitbucket.org/uadigital/ua-bootstrap#tags" target="_blank">Tagged Releases
          </a></li>
    </ul>
  </div>
</footer>
